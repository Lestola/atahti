//
//  render.hpp
//  Atahti
//
//  Created by Marko Lehtola on 19/09/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef render_hpp
#define render_hpp

#include <stdio.h>
#include "Struct.h"
#include <vector>

class Render{
    
public:
    
    void renderRoute(int* data, node* t);
    
    void setHeightOfMap (int height){
        this->heightOfMap = height;
    }
    void setWidthOfMap (int width){
        this->widthOfMap = width;
    }
    
    void setEndCoordinates (int xCoord, int yCoord){
        this->endNodeXCoordinate = xCoord;
        this->endNodeYCoordinate = yCoord;
    }
    
    void setStartCoordinates (int xCoord, int yCoord){
        this->startNodeXCoordinate = xCoord;
        this->startNodeYCoordinate = yCoord;
    }
    
private:
    int heightOfMap;
    int widthOfMap;
    int endNodeXCoordinate;
    int endNodeYCoordinate;
    int startNodeXCoordinate;
    int startNodeYCoordinate;
    
};

#endif /* render_hpp */
