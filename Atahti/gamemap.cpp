//
//  gamemap.cpp
//  Algorithms
//
//  Created by Teemu Saarelainen on 23/11/2017.
//  Copyright © 2017 Teemu Saarelainen. All rights reserved.
//

#include "gamemap.hpp"
#include <iostream>
#include <fstream>
#include <string>

GameMap::GameMap(std::string filename){
    
    std::string line;
    std::ifstream myfile (filename);
    if (myfile.is_open()){
        
        // haetaan tiedostosta korkeus H ja leveys W
        //------------------------------------------
        if (getline (myfile,line)){
            // set W as the first integer until ','
            
            // Use find_first_of:
            // find_first_of (str,pos)
            unsigned long index = line.find_first_of(",");
            
            // Use substr:
            // substr (pos, len)
            this->width = std::stoi(line.substr(0,index));
            //std::cout << "W: " << this->width << std::endl;
            
            // set H as the second integer
            this->height = std::stoi(line.substr(index+1));
            //std::cout << "H: " << this->height << std::endl;
        }
        else {
            std::cout << "Error reading file!\n";
            return;
        }
        
        // haetaan int taulukkoon "data", tiedostoon tallennetut merkit
        //-------------------------------------------------------------
        this->data = new int[this->width*this->height];
        
        // read H more lines and put data into the map
        for (int y=0;y<this->height;y++){
            // remember to catch error on this!!!
            if (getline (myfile,line)){
                for (int x = 0; x < this->width; x++){
                    this->data[y*this->width + x] = std::stoi(line.substr(2*x,1));
                }
            }
            else {
                std::cout << "Error reading file!\n";
                return;
            }
        }
        
        myfile.close(); //suljetaan tiedosto
    }
    else{
        std::cout << "Unable to open file\n";
        return;
    }
    
}
