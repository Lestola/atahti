//
//  gamemap.hpp
//  Algorithms
//
//  Created by Teemu Saarelainen on 23/11/2017.
//  Copyright © 2017 Teemu Saarelainen. All rights reserved.
//

#ifndef gamemap_hpp
#define gamemap_hpp

#include <stdio.h>
#include <string>
#include <iostream>

class GameMap {
    
public:
    
    GameMap(std::string filename);
    
    int* getMap(){
   
        return data;
    };
    
    int getWidth(){
        return width;
    };
    
    int getHeight(){
        return height;
    }
    
private:
    
    int width, height;
    int* data;
    
};

#endif /* gamemap_hpp */
