//
//  Struct.h
//  Atahti
//
//  Created by Marko Lehtola on 21/09/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef Struct_h
#define Struct_h

struct node {
    int x;          //x-koordinaatti
    int y;          //y-koordinaatti
    int Gcost;      //kuinka pitkällä ollaan aloituspisteest
    int Hcost;      //kuinka pitkällä ollaan lopetuspisteestä
    int Fcost;      //äskeisten yhteenlaskettu
    int Place;      //paikka ruudukolla
    node* parent;   //tämän noden vanhempi
} ;

#endif /* Struct_h */
