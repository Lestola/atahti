//
//  render.cpp
//  Atahti
//
//  Created by Marko Lehtola on 19/09/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "render.hpp"
#include <iostream>
#include "Struct.h"
#include <vector>

void Render::renderRoute(int *data, node* t){
    //aloitetaan kartan tulostus
    for (int j=0;j<heightOfMap;j++){
        
        //tulostetaan katto ruuduille
        for (int i=0;i<widthOfMap;i++){
            std::cout << "+---------";
        }
        std::cout << std::endl;
        
        //tulostetaan onko seinä, aloituspaikka, lopetuspaikka, reittipiste tai tyhjä ruutu
        for (int i=0;i<widthOfMap;i++){
            if (data[j*widthOfMap+i] == 0){
                //seinä
                std::cout << "||||||||||";
            } else {
                if (i == startNodeXCoordinate-1 && j == startNodeYCoordinate-1){
                    //aloituspaikka
                    std::cout << "|    X    ";
                } else if (i == endNodeXCoordinate-1 && j == endNodeYCoordinate-1){
                    //lopetuspaikka
                    std::cout << "|    O    ";
                } else {
                    bool notYetFound = true;
                    node* test = t;
                    do{
                        if (test->x == i+1 && test->y == j+1){
                            //reittipiste
                            std::cout << "|    *    ";
                            notYetFound = false;
                        }
                        test = test->parent;
                    }while (test->parent != NULL);
                    
                    if(notYetFound){
                        //tyhjä ruutu
                        std::cout << "|         ";
                    }
                }
            }
        }
        std::cout << std::endl;
    }
    
    //tulostetaan lattia ruuduille
    for (int i=0;i<widthOfMap;i++){
        std::cout << "+---------";
    }
};
