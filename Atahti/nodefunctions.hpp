//
//  nodefunctions.hpp
//  Atahti
//
//  Created by Marko Lehtola on 08/10/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#ifndef nodefunctions_hpp
#define nodefunctions_hpp

#include <stdio.h>
#include <vector>
#include "Struct.h"

int nodePlace(int x, int y);

std::vector< node* > createNode (int x, int y, int xModifier, int yModifier, int x1, int y1, node* n, node* t, node* h,
                                 std::vector< node* > openNODES,std::vector< node* > closedNODES, int amountToAddGCost);

std::vector< node* > calculateNewNodes(int tasonLoppuX, int tasonLoppuY, node* n,node* t,node* h,std::vector< node* > openNODES,std::vector< node* > closedNODES);


#endif /* nodefunctions_hpp */
