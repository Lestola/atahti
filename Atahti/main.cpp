//
//  main.cpp
//  A* pathfinding
//
//  Created by Marko Lehtola on 19/02/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include <iostream>
#include "nodefunctions.hpp"
#include "gamemap.hpp"
#include "render.hpp"
#include <vector>
#include <stdio.h>
#include <math.h>
#include "Struct.h"



int main(int argc, const char * argv[]) {
    //------------------------------------------
    //ladataan kartta
    //------------------------------------------
    GameMap kartta("/Volumes/ApuLevy/Ohjelmointi/Atahti/Atahti/kartta.txt");
    
    //------------------------------------------
    //luodaan muuttujat
    //------------------------------------------
    int* mapData = kartta.getMap();     //kartan data
    int mapHeight = kartta.getHeight(); //kartan korkeus
    int mapWidth = kartta.getWidth();   //kartan leveys
    int x0 = 10;                        //lähtö x
    int y0 = 9;                         //lähtö y
    int x1 = 7;                         //loppu x
    int y1 = 6;                         //loppu y
    std::vector< node* > openNODES;     //vektori array avoimista nodeista
    std::vector< node* > closedNODES;   //vektori array suljetuista nodeista
    node* n;                            //uuden noden pointteri
    node* t;                            //väliaikainen pointteri
    node* h;                            //aloitus noden pointteri
    bool finishNotYetAchieved = true;   //ohjelma tekee uusia nodeja kunnes tämän muuttujan arvo on false
    
    //------------------------------------------
    //pyydetään käyttäjää antamana koordinaatit
    //------------------------------------------
    std::cout << "Suositellut koordinaatit:" << std::endl << "Lähtö X:10, Lähtö Y:9, Loppu X:7, Loppu Y: 6 (seinän sisään sijoitetut koordinaatit kaatavat kartanpiirto koodin)" << std::endl << std::endl;
    std::cout << "Anna lähtö X:" << std::endl;
    std::cin >> x0;
    std::cout << "Anna lähtö Y:" << std::endl;
    std::cin >> y0;
    std::cout << "Anna loppu X:" << std::endl;
    std::cin >> x1;
    std::cout << "Anna loppu X:" << std::endl;
    std::cin >> y1;
    
    //------------------------------------------
    //luodaan kartan renderöintikone ja asetetaan sen parametrit
    //------------------------------------------
    Render Renderer;
    Renderer.setHeightOfMap(mapHeight);
    Renderer.setWidthOfMap(mapWidth);
    Renderer.setEndCoordinates(x1, y1);
    Renderer.setStartCoordinates(x0, y0);
    
    //----------------------------------------------------
    //luodaan aloitus pointteri paikkaan josta aloitetaan
    //----------------------------------------------------
    
    n = new node;
    closedNODES.push_back(n);
    n->x = x0;
    n->y = y0;
    n->Place = ((y0-1)*11) + x0;
    n->Hcost = sqrt (((n->x*10 - x1*10)*(n->x*10 - x1*10)) + ((n->y*10 - y1*10)*(n->y*10 - y1*10)));
    n->Gcost = 0;
    n->Fcost = n->Hcost + n->Gcost;
    t=n;
    h=n;
    n->parent = NULL;
    openNODES = calculateNewNodes(x1,y1,n,t,h,openNODES,closedNODES); //lasketaan ensimmäiset nodet aloituspaikan ympärillä
    
    //-----------------------------------------------
    //Tutkitaan uusia Nodeja kunnes ollaan maalissa!
    //-----------------------------------------------
    
    do{
        
    if (t->x == x1 && t->y == y1){ //----MAALISSA----
        
        //tulostetaan tiedote, että ollaan maalissa
        std::cout << std::endl << std::endl << "Lyhin reitti O:ltä X:lle:" << std::endl;
        
        //tulostetaan lyhin reitti x-y koordinaatein käyttäjälle
        node* seurattavaNode = t;
        do{
            std::cout << "X:" << seurattavaNode->x << ", Y:" << seurattavaNode->y << std::endl;
            seurattavaNode = seurattavaNode->parent;
        } while (seurattavaNode->parent != NULL);
        
        //tulostetaan visuaalinen kartta käyttäjälle
        Renderer.renderRoute(mapData, t);
        
        //muutetaanmuuttuja niin, että ollaan saavutettu maali ja looppi voidaan lopettaa
        finishNotYetAchieved = false;
        
    } else { //----EI MAALISSA----
        
        //etsitään halvin node etenemiseen
        node* cheapestNode = openNODES.front(); //luodaan halvin node ja laitetaan siihen alustavasti ensimmäisenä listalla oleva
        int numberOfCheapestNode = 0; //muuttuja sille, missä openNODES "slotissa" halvin node on
            
        for (int x= 0;x<openNODES.size();x++){
            //etsitään node, jossa Fcost on pienin, ja valitaan se seuraavaksi nodeksi
            if (openNODES[x]->Fcost < cheapestNode->Fcost){
                cheapestNode = openNODES[x];
                numberOfCheapestNode = x; //tallennetaan tieto missä openNODES  sijainnissa halvin node oli
            } else if (openNODES[x]->Fcost == cheapestNode->Fcost){ //jos useammassa Fcosti sama
                if (openNODES[x]->Hcost < cheapestNode->Hcost){     //valitaan se, jossa Hcost pienen
                    cheapestNode = openNODES[x];
                    numberOfCheapestNode = x; //tallennetaan tieto missä openNODES  sijainnissa halvin node oli
                }
            }
        }
        
        //asetetaan halvin node temp nodeksi, jotta uudet luodut nodet linkitetään tähän kyseiseen nodeen
        t=cheapestNode;
            
        //lasketaan uudet nodet halvimman noden ympäriltä
        openNODES = calculateNewNodes(x1,y1,n,t,h,openNODES,closedNODES);
            
        //siirretään halvin node suljetulle listalle ja poistetaan avoimien listalta
        closedNODES.push_back(t);
        openNODES.erase(openNODES.begin() + numberOfCheapestNode);
        }
        
    } while (finishNotYetAchieved);
    
    //------------------------------------------
    //lopetetaan ohjelma
    //------------------------------------------
    std::cout << std::endl;
    return 0;
}


