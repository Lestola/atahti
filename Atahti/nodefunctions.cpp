//
//  nodefunctions.cpp
//  Atahti
//
//  Created by Marko Lehtola on 08/10/2019.
//  Copyright © 2019 Marko Lehtola. All rights reserved.
//

#include "nodefunctions.hpp"
#include "gamemap.hpp"
#include <math.h>

int nodePlace(int x, int y){
    int place;
    place = (((y-1)*11) + x);
    return place;
}

std::vector< node* > createNode (int x, int y, int xModifier, int yModifier, int x1, int y1, node* n, node* t, node* h,
                                 std::vector< node* > openNODES,std::vector< node* > closedNODES, int amountToAddGCost){
    
    bool newNodeIsAllowed = true;
    //--------------------------------------------------------
    //Tarkistetaan onko uusi Node kartan sisäpuolella
    //--------------------------------------------------------
    
    //ladataan kartta tiedostosta
    GameMap kartta("/Volumes/ApuLevy/Ohjelmointi/Atahti/Atahti/kartta.txt");
       
    //haetaan kartan korkeus ja leveys
    int mapHeight = kartta.getHeight();
    int mapWidth = kartta.getWidth();
       
    if (x+xModifier < 1 || x+xModifier > mapWidth){
        //std::cout << "node meinasi mennä alueen ulkopuolelle" << std::endl;
        newNodeIsAllowed = false;
    }
    if (y+yModifier < 1 || y+yModifier > mapHeight){
        //std::cout << "node meinasi mennä alueen ulkopuolelle" << std::endl;
        newNodeIsAllowed = false;
    }
    
    //--------------------------------------------------------
    //Tarkistetaan onko seinän sisällä
    //--------------------------------------------------------
       int* karttaData = kartta.getMap();
       if (karttaData[(y+yModifier-1)*mapWidth+(x+xModifier-1)] == 0){
           //std::cout << "Löytyi seinä("<< x+xModifier << "," << y+yModifier << ")" << std::endl;
           newNodeIsAllowed = false;
       }
    
    //--------------------------------------------------------
    //tarkistetaan ettei löydy suljetuista nodeista
    //--------------------------------------------------------
    for(int i=0;i<closedNODES.size();i++){
        if(nodePlace(x+xModifier, y+yModifier) == closedNODES[i]->Place){
            newNodeIsAllowed = false;
        }
    }
    
    //--------------------------------------------------------
    //tarkistetaan ettei löydy aukinaisista nodeista
    //--------------------------------------------------------
    
    for(int i=0;i<openNODES.size();i++){
        if(nodePlace(x+xModifier, y+yModifier) == openNODES[i]->Place){
            newNodeIsAllowed = false;
        }
    }
    
    //--------------------------------------------------------
    //Luodaan uusi node, jos se läpäisee kaikki testit
    //--------------------------------------------------------
    if(newNodeIsAllowed){
        n = new node;
        n->x = x+xModifier;                    //pysytään koordinaatistossa samalla columnilla
        n->y = y+yModifier;                    //siirrytään koordinaatistossa yksi rivi ylemmäs
        int xWithModifier = x+xModifier;
        int yWithModifier = y+yModifier;
        n->Place = ((yWithModifier-1)*11) + xWithModifier;
        n->Gcost = (t->Gcost + amountToAddGCost);     //^
        n->Hcost = sqrt (((n->x*10 - x1*10)*(n->x*10 - x1*10)) + ((n->y*10 - y1*10)*(n->y*10 - y1*10)));
        n->Fcost = n->Hcost + n->Gcost;
        n->parent = t;
        openNODES.push_back(n);
    }
    
    //--------------------------------------------------------
    //palautetaan lista nodeista
    //--------------------------------------------------------
    return openNODES;
}

std::vector< node* > calculateNewNodes(int tasonLoppuX, int tasonLoppuY, node* n,node* t,node* h,std::vector< node* > openNODES,std::vector< node* > closedNODES){
    
    //------------------------------------------
    //Luodaan uudet nodet edellisen ympärille
    //------------------------------------------
    
    //YLÄ-KESKI
    int xModifier = 0;
    int yModifier = -1;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,10);
    
    
    //ALA-KESKI
    xModifier = 0;
    yModifier = +1;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,10);

    
    //VASEN
    xModifier = -1;
    yModifier = 0;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,10);
    
    //OIKEA
    xModifier = +1;
    yModifier = 0;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,10);
    
    
    //YLÄ-OIKEA
    xModifier = +1;
    yModifier = -1;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,14);
    
    //YLÄ-VASEN
    xModifier = -1;
    yModifier = -1;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,14);
    
    //ALA-VASEN
    xModifier = -1;
    yModifier = +1;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,14);
    
    //ALA-OIKEA
    xModifier = +1;
    yModifier = +1;
    openNODES = createNode(t->x, t->y, xModifier, yModifier, tasonLoppuX, tasonLoppuY, n, t, h, openNODES,closedNODES,14);
    
    //------------------------------------------
    //Palautetaan nodet main ohjelmalle
    //------------------------------------------
    return openNODES;
}
